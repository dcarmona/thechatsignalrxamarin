﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheChat.Client.Helpers;
using TheChat.Core.Services;
using TheChat.Messages;

namespace TheChat.Client
{
    class Program
    {
        static ChatService service;
        static string userName;
        static string room;
        static async Task Main(string[] args)
        {
            Console.WriteLine("User Name: ");
            userName = Console.ReadLine();

            service = new ChatService();
            service.OnReceivedMessage += Service_OnReceivedMessage;
            await service.InitAsync(userName);
            Console.WriteLine("You are now connected");

            await JoinRoom();

            bool keepGoing = true;
            do
            {
                string text = Console.ReadLine();
                if (text.Equals("exit"))
                {
                    await service.DisconnectAsync();
                    keepGoing = false;
                }
                else if (text.Equals("leave"))
                {
                    var message = new UserConnectedMessage(userName, room);
                    await service.LeaveChannelAsync(message);
                    await JoinRoom();
                }
                else if (text.Equals("private"))
                {
                    Console.WriteLine("Enter UserName: ");
                    var user = Console.ReadLine();
                    Console.WriteLine("Enter private message: ");
                    text = Console.ReadLine();
                    var message = new SimpleTextMessage(userName)
                    {
                        Text = text,
                        Recipient = user
                    };
                    await service.SendMessageAsync(message);
                }
                else if (text.Equals("image")) 
                {
                    var imagePath = @"E:\imagestest\daniel.jpg";
                    var imageStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                    var bytes = ImageHelper.ReadFully(imageStream);
                    var base64Photo = Convert.ToBase64String(bytes);
                    var message = new PhotoMessage(userName)
                    {
                        Base64Photo = base64Photo,
                        FileEnding = imagePath.Split('.').Last(),
                        GroupName = room
                    };

                    await service.SendMessageAsync(message);
                }
                else
                {
                    var message = new SimpleTextMessage(userName)
                    {
                        Text = text,
                        GroupName = room
                    };
                    await service.SendMessageAsync(message);
                }
            } while (keepGoing);


        }

        private static async Task JoinRoom()
        {
            var rooms = await service.GetRooms();
            foreach (var room in rooms)
            {
                Console.WriteLine(room.Name);
            }

            room = Console.ReadLine();

            var message = new UserConnectedMessage(userName, room);
            await service.JoinChannelAsync(message);

            var usersInRoom = await service.GetUsersGroup(room);
            Console.WriteLine($"There are currently {usersInRoom.Count} users in the room.");
        }

        private static void Service_OnReceivedMessage(object sender, Core.EventHandlers.MessageEventArgs e)
        {
            if (e.Message.Sender == userName)
                return;

            if (e.Message.TypeInfo.Name == nameof(SimpleTextMessage))
            {
                var simpleText = e.Message as SimpleTextMessage;
                var message = $"{simpleText.Sender}: {simpleText.Text}";
                Console.WriteLine(message);
            }
            else if (e.Message.TypeInfo.Name == nameof(UserConnectedMessage))
            {
                var userConnected = e.Message as UserConnectedMessage;
                string message = string.Empty;
                if (userConnected.IsEntering)
                {
                    message = $"{userConnected.Sender}: has connected.";
                }
                else
                {
                    message = $"{userConnected.Sender}: has left.";
                }

                Console.WriteLine(message);
            } else if (e.Message.TypeInfo.Name == nameof(PhotoUrlMessage)) 
            {
                var photoMessage = e.Message as PhotoUrlMessage;
                string message = $"{photoMessage.Sender} sent {photoMessage.Url}";
                Console.WriteLine(message);
            }
        }
    }
}
