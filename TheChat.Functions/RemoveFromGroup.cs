using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using TheChat.Functions.Models;
using TheChat.Messages;

namespace TheChat.Functions
{
    public static class RemoveFromGroup
    {
        [FunctionName("RemoveFromGroup")]
        public static async Task Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            [SignalR(HubName = "chat")] IAsyncCollector<SignalRGroupAction> signalRGroupActions,
            [Table("Users", Connection = "StorageConnection")] CloudTable usersTable,
            ILogger log)
        {
            UserConnectedMessage message = new JsonSerializer()
                .Deserialize<UserConnectedMessage>(new JsonTextReader(new StreamReader(req.Body)));

            await signalRGroupActions.AddAsync(new SignalRGroupAction
            {
                ConnectionId = message.Token,
                UserId = message.Sender,
                GroupName = message.GroupName,
                Action = GroupAction.Remove
            });

            TableQuery<UserEntity> rangeQuery = new TableQuery<UserEntity>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, message.Sender));

            foreach (var entity in await usersTable.ExecuteQuerySegmentedAsync(rangeQuery, null))
            {
                TableOperation deleteOperation = TableOperation.Delete(entity);
                TableResult result = await usersTable.ExecuteAsync(deleteOperation);
            }
        }
    }
}

