using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using TheChat.Functions.Models;

namespace TheChat.Functions
{
    public static class User
    {
        [FunctionName("User")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "User/{userId}")] HttpRequest req,
            [Table("Users", Connection = "StorageConnection")] CloudTable usersTable,
            string userId,
            ILogger log)
        {
            TableQuery<UserEntity> rangeQuery = new TableQuery<UserEntity>()
                .Where(
                    TableQuery.GenerateFilterCondition("RowKey",
                    QueryComparisons.Equal,
                    userId)
                );

            List<UserEntity> users = (from entity in await usersTable.ExecuteQuerySegmentedAsync(rangeQuery, null)
                                      select entity).ToList();

            return new OkObjectResult(users);
        }
    }
}

