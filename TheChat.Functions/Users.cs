using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using TheChat.Functions.Models;
using System.Linq;

namespace TheChat.Functions
{
    public static class Users
    {
        [FunctionName("Users")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "Users/{room}")] HttpRequest req,
            [Table("Users", Connection = "StorageConnection")] CloudTable usersTable,
            string room,
            ILogger log)
        {
            TableQuery<UserEntity> rangeQuery = new TableQuery<UserEntity>()
                .Where(
                    TableQuery.GenerateFilterCondition("PartitionKey", 
                    QueryComparisons.Equal,
                    room)
                );

            List<UserEntity> users = (from entity in await usersTable.ExecuteQuerySegmentedAsync(rangeQuery, null)
                                      select entity).ToList();

            return new OkObjectResult(users);
        }
    }
}

